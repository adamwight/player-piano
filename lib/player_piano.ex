# TODO:
#   * absolute dimensions
defmodule PlayerPiano do
  alias PlayerPiano.Midi.Reader
  alias PlayerPiano.Disc.Writer

  @spec run(String.t(), String.t()) :: :ok
  def run(in_path, out_path) do
    Reader.read(in_path)
    |> Writer.write(out_path)
  end
end
