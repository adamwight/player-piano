defmodule PlayerPiano.Disc do
  @moduledoc """
  Specific music box that takes 177.3mm discs, has a C-A'' range, and a hard
  time repeating notes quickly.
  """

  alias PlayerPiano.Midi.Note, as: Note

  defmodule Writer do
    alias PlayerPiano.Disc.{DesignRules, Mapper, Renderer}

    @spec write([Note], String.t()) :: :ok
    def write(notes, path) do
      notes
      |> Mapper.calculate_arcs()
      |> DesignRules.check()
      |> Renderer.render_svg()
      |> then(&File.write!(path, &1))
    end
  end

  defmodule Harp do
    @spec pitches :: [integer()]
    def pitches() do
      [
        # Note 60 = Middle C, note 93 = A''
        60,
        62,
        64,
        65,
        67,
        69,
        71,
        72,
        74,
        76,
        77,
        79,
        81,
        83,
        84,
        86,
        88,
        89,
        91,
        93
      ]
    end

    @spec note_index(integer()) :: integer() | nil
    def note_index(note),
      do: Enum.find_index(pitches(), fn x -> note_normalize(note) == x end)

    # TODO: more methodical way of doing this.  Warn when wrapping.
    defp note_normalize(note) when note < 60, do: 60 + rem(note, 12)

    defp note_normalize(note) when note > 93, do: 72 + rem(note, 12)

    defp note_normalize(note), do: note
  end

  defmodule Mapper do
    @first_radius 23.9
    @last_radius 81.0
    @two_pi 2 * :math.pi()
    @cycle_duration_ms 28_000

    @spec calculate_arcs([Note]) :: list
    def calculate_arcs(notes) do
      notes
      |> Enum.map(fn event ->
        %{
          radius: note_radius(event.pitch),
          angle: time_angle(event.start_time),
          # And the rest is for debugging:
          pitch: event.pitch
          # measure: div(event.start_time, 480 * 4) + 1,
          # beat: rem(event.start_time, 480 * 4) / 480
        }
      end)
      |> optimize()
    end

    defp note_radius(note) do
      note
      |> Harp.note_index()
      |> harp_note_radius()
    end

    defp harp_note_radius(nil), do: raise("Cannot render note")

    defp harp_note_radius(index),
      do: @first_radius + index * note_spacing()

    defp time_angle(time), do: @two_pi * time / @cycle_duration_ms

    # TODO: optimize head travel
    defp optimize(coords), do: Enum.sort_by(coords, & &1.angle)

    @spec note_spacing :: float
    def note_spacing(), do: (@last_radius - @first_radius) / (Enum.count(Harp.pitches()) - 1)
  end

  defmodule Renderer do
    # dims in mm, calibrated to svg
    @border_radius 89
    @center_hole 2.63
    @note_height_duty 0.6
    @note_sweep 2
    @timing_radius 86.15
    @timing_count 143
    @timing_duty 0.55
    @timing_height 2.5
    @stroke "black"

    @two_pi 2 * :math.pi()

    @spec render_svg(list) :: String.t()
    def render_svg(arcs) do
      """
      <svg version="1.2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
        viewBox="0 0 200 200"
        style="width: 200mm; height: 200mm;"
      >

      <circle cx="100" cy="100" r="<%= @border_radius %>" fill="none" <%= @stroke_attribs %> />
      <circle cx="100" cy="100" r="<%= @center_hole %>" fill="none" <%= @stroke_attribs %> />

      <%= for note <- @notes do %>
        <%= note %>
      <% end %>

      <%= @timing %>

      </svg>
      """
      |> EEx.eval_string(
        assigns: %{
          border_radius: @border_radius,
          center_hole: @center_hole,
          stroke_attribs: stroke_attribs(),
          notes: Enum.map(arcs, &note_svg/1),
          timing: render_timing()
        }
      )
    end

    defp stroke_attribs(),
      do: """
        stroke="#{@stroke}" stroke-width="1" vector-effect="non-scaling-stroke"
      """

    # stroke="#{@stroke}" stroke-width="1" vector-effect="non-scaling-stroke"
    # stroke="#{@stroke}" stroke-width="#{@hairline}"
    # stroke="#{@stroke}" stroke-width="0.1"
    # shape-rendering="crispEdges"

    defp render_timing() do
      1..@timing_count
      |> Enum.map(&timing_notch/1)
    end

    defp timing_notch(index) do
      spacing = @two_pi / @timing_count
      notch(@timing_radius, index * spacing, @timing_height, spacing * @timing_duty)
    end

    defp note_svg(note) do
      note_height = @note_height_duty * Mapper.note_spacing()

      notch(note.radius, note.angle, note_height, @note_sweep / note.radius)
    end

    defp polar_cartesian(r, angle), do: [100 + r * :math.cos(angle), 100 - r * :math.sin(angle)]

    defp notch(radius, angle, height, sweep) do
      # params: radius of middle; leading edge angle
      r_inner = radius - height / 2
      r_outer = radius + height / 2
      angle_begin = angle
      angle_end = angle + sweep
      [xi0, yi0] = polar_cartesian(r_inner, angle_begin)
      [xie, yie] = polar_cartesian(r_inner, angle_end)
      [xo0, yo0] = polar_cartesian(r_outer, angle_begin)
      [xoe, yoe] = polar_cartesian(r_outer, angle_end)

      """
      <path fill="none" <%= @stroke_attribs %> d="
        M <%= @xi0 %> <%= @yi0 %>
        A <%= @r_inner %> <%= @r_inner %> 0 0 0 <%= @xie %> <%= @yie %>
        L <%= @xoe %> <%= @yoe %>
        A <%= @r_outer %> <%= @r_outer %> 0 0 1 <%= @xo0 %> <%= @yo0 %>
        L <%= @xi0 %> <%= @yi0 %>
      "/>
      """
      |> EEx.eval_string(
        assigns: %{
          xi0: xi0,
          yi0: yi0,
          xo0: xo0,
          yo0: yo0,
          xie: xie,
          yie: yie,
          xoe: xoe,
          yoe: yoe,
          r_inner: r_inner,
          r_outer: r_outer,
          stroke_attribs: stroke_attribs()
        }
      )
    end
  end

  defmodule DesignRules do
    @moduledoc """
    This mechanism can only repeat a notes once that note's pawl has reset,
    so calculate the required margin and flag any repeating notes too close to
    be played.  Without this rule, the player will miss these subsequent notes.
    The reset timing is large for this device, around a quarter note for most
    tempos, and varies according to the radius and pitch.
    """

    @note_repeat_margin 7.5

    @spec check(list) :: list
    def check(coords) do
      coords
      |> Enum.reduce(%{}, fn coord, margin ->
        # Debugging
        # IO.inspect("#{coord.pitch} at #{coord.angle}")

        case margin[coord.pitch] do
          # Note there is an optimization to ignore > 2pi.  FIXME: Needs to do an
          # additional pass to check against end and beginning of song.
          # TODO: accumulate errors rather than raise
          earliest when earliest > coord.angle and not is_nil(earliest) ->
            raise("too soon: #{inspect(coord)}, earliest #{inspect(earliest)}")

          _ ->
            Map.put(margin, coord.pitch, add_margin(coord))
        end
      end)

      coords
    end

    defp add_margin(coord) do
      coord.angle + @note_repeat_margin / coord.radius
    end
  end
end
