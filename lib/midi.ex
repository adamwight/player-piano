defmodule PlayerPiano.Midi do
  @moduledoc """
  Read MIDI files, collapsing tracks and simplifying into a list of notes.
  """

  defmodule Note do
    defstruct [:start_time, :pitch]
    # No need for duration or velocity.
  end

  defmodule Reader do
    @spec read(String.t()) :: [Note]
    def read(path) do
      path
      |> Midifile.read()
      |> Map.get(:tracks)
      |> Enum.reduce(%{time: nil, notes: []}, &extract_track_notes/2)
      |> Map.get(:notes)
    end

    defp extract_track_notes(track, result) do
      # Restart timer for each track
      result = %{result | time: 0}

      track.events
      |> Enum.reduce(result, &extract_event/2)
    end

    defp extract_event(event, result) do
      result = %{result | time: result.time + ticks_ms(event.delta_time)}

      case event.symbol do
        # TODO: absolute time
        :on ->
          [_channel, note, _velocity] = event.bytes
          %{result | notes: result.notes ++ [%Note{start_time: result.time, pitch: note}]}

        _ ->
          result
      end
    end

    @ppq 480
    @tempo 90

    # See MIDI spec p. 143
    # TODO: dynamic tempo changes
    # FIXME: read tempo from MIDI, caller tweaks slightly to match loop points
    # (although these aren't in the MIDI standard)
    defp ticks_ms(ticks), do: ticks * 60_000 / (@ppq * @tempo)
  end
end
