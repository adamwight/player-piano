example.svg: example.midi
	elixir -S mix run -e 'PlayerPiano.run("$<", "$@")'

example.midi: example.ly
	lilypond $<
