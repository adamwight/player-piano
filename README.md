Generate discs for a specific music box.

## Installing

You'll need Elixir 1.12 or newer, then:

    mix deps.get

## Running

To use the defaults, building the `example.ly` file into `example.svg`:

    make

To parse raw MIDI files,

    elixir -S mix run -e 'PlayerPiano.run("input.midi", "out.svg")'
